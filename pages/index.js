import { useEffect, useState } from "react";
import Head from "next/head";
import { useTheme } from "next-themes";
import ProductWidget from "./ProductWidget";

export default function Home() {
  const AVALIBLE_PRODUCTS = [
    {
      backgroundColor: "yellow",
      top: {
        feature: "In-Person Access",
        title: {
          name: "Boxful Self-Storage",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Full control of your belongings at Boxful Stores",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "Store Locations",
          description: "24hr private access to your belongings",
          price: "from $494 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
          extension: {
            type: "table",
            data: [],
          },
        },
      ],
      bottom: {
        list: [
          "1-month min storage period",
          "No deposit required",
          "Best price guarantee",
        ],
        buttonText: "SEE STORES",
      },
    },
    {
      backgroundColor: "green",
      top: {
        feature: "We Come To You",
        title: {
          name: "Boxful Door-to-Door",
          url: "https://www.boxful.com/en/self-storage",
        },
        description: "Effortless storage experience at home",
        image:
          "https://www.boxful.com.tw/images/new-home/locker.png.pagespeed.ce.Fn_N6TvTU9.png",
      },
      main: [
        {
          title: "By-The-Item",
          description: "24hr private access to your belongings",
          price: "from $29 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
        {
          title: "By-Square-Feet",
          description: "24hr private access to your belongings",
          price: "from $349 /month",
          priceDescription: "1 free access per month, 1 free pickup trip",
        },
      ],
      bottom: {
        list: [
          "We move & deliver for you",
          "Pay monthly for what you use",
          "6Easy online booking & management",
        ],
        buttonText: "SEE PLANS",
      },
    },
  ];
  const [isMounted, setIsMounted] = useState(false);
  const { theme, setTheme } = useTheme();
  const switchTheme = () => {
    if (isMounted) {
      setTheme(theme === "light" ? "dark" : "light");
    }
  };

  useEffect(() => {
    setIsMounted(true);
  }, []);

  return (
    <>
      <button
        class="py-3 px-6 bg-indigo-100 hover:bg-indigo-200 cursor-pointer rounded-3xl"
        onClick={switchTheme}
      >
        CHANGE THEME
      </button>
      <div class="bg-white text-black dark:bg-black dark:text-white container mx-auto max-w-screen-xl flex flex-col justify-between md:flex-row">
        {AVALIBLE_PRODUCTS.map((product) => (
          <ProductWidget data={product} />
        ))}
      </div>
    </>
  );
}
